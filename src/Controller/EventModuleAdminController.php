<?php

namespace App\Controller;

use App\Model\Priority\PriorityHandler;
use App\Repository\EventModuleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EventModuleAdminController extends Controller
{
    /**
     * @param $id
     * @param EventModuleRepository $moduleRepository
     * @param PriorityHandler $priorityHandler
     * @param ObjectManager $manager
     * @return RedirectResponse
     */
    public function priorityUpAction(
        $id,
        EventModuleRepository $moduleRepository,
        PriorityHandler $priorityHandler,
        ObjectManager $manager)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        $priorityHandler->changePriorityAction($moduleRepository, $manager, $object, -1);;

        return new RedirectResponse($this->admin->generateUrl('list'));

    }

    /**
     * @param $id
     * EventModuleRepository $moduleRepository
     * @param EventModuleRepository $moduleRepository
     * @param PriorityHandler $priorityHandler
     * @param ObjectManager $manager
     * @return RedirectResponse
     */
    public function priorityDownAction(
        $id,
        EventModuleRepository $moduleRepository,
        PriorityHandler $priorityHandler,
        ObjectManager $manager)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        $priorityHandler->changePriorityAction($moduleRepository, $manager, $object, 1);

        return new RedirectResponse($this->admin->generateUrl('list'));

    }

}