<?php


namespace App\Controller;


use App\Repository\NewsModuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends Controller
{

    /**
     * @Route("news", name="app_page_news")
     * @param NewsModuleRepository $newsModuleRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newsAction(NewsModuleRepository $newsModuleRepository){

        $news = $newsModuleRepository->getHotNews();

        return $this->render('news/page_news.html.twig',[
            'news' => $news
        ]);
    }

    /**
     * @Route("news/{id}", name="app_single_news")
     * @param NewsModuleRepository $newsModuleRepository
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function singleNewsAction(NewsModuleRepository $newsModuleRepository, int $id){

        $singleNews = $newsModuleRepository->find($id);


        return $this->render('news/single_news.html.twig',[
            'singleNews' => $singleNews
        ]);
    }

}