<?php


namespace App\Controller;


use App\Model\Navigation\NavigationHandler;
use App\Repository\DynamicPageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class NavigationController extends Controller
{
    /**
     * @Route("/navigation", name="app_navigation")
     * @param NavigationHandler $navigationHandler
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param DynamicPageRepository $dynamicPageRepository
     */
    public function showNavigationAction(NavigationHandler $navigationHandler)
    {
        $rootLinks = $navigationHandler->getLinksForMainMenu();

        return $this->render('navigation/main_menu.html.twig', [
            'root_links' => $rootLinks
        ]);
    }

    /**
     * @Route("/footer-navigation", name="app_navigation_footer")
     * @param DynamicPageRepository $dynamicPageRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showFooterNavigationAction(DynamicPageRepository $dynamicPageRepository)
    {
        $rootLinks = $dynamicPageRepository->getRootNavigationLinks();

        return $this->render('navigation/footer_menu.html.twig', [
            'root_links' => $rootLinks
        ]);
    }


    /**
     * @Route("/sidebar-navigation/{path}", name="app_navigation_sidebar", requirements={"route"=".+"})
     * @param NavigationHandler $navigationHandler
     * @param string $path
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param DynamicPageRepository $dynamicPageRepository
     */
    public function showSidebarNavigationAction(NavigationHandler $navigationHandler, string $path)
    {
        $links = $navigationHandler->getSidebarNavigation($path);

        return $this->render('navigation/sidebar_menu.html.twig', [
            'links' => $links
        ]);
    }

    /**
     * @Route("/sidebar-event/", name="app_event_sidebar")
     * @param NavigationHandler $navigationHandler
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param DynamicPageRepository $dynamicPageRepository
     */
    public function showSidebarForEventAction(NavigationHandler $navigationHandler)
    {
        $links = $navigationHandler->getLinksForEventSidebar();

        return $this->render('navigation/sidebar_menu.html.twig', [
            'links' => $links
        ]);
    }



    /**
     * @Route("/breadcamps-navigation/{path}", name="app_navigation_breadcamps")
     * @param NavigationHandler $navigationHandler
     * @param string $path
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param DynamicPageRepository $dynamicPageRepository
     */
    public function showBreadCampsNavigationAction(NavigationHandler $navigationHandler, string $path)
    {

        $links = $navigationHandler->getBreadCamps($path);
        return $this->render('navigation/breadcamps_menu.html.twig', [
            'links' => $links
        ]);
    }

}