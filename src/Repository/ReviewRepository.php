<?php

namespace App\Repository;

use App\Entity\ReviewModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReviewModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReviewModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReviewModule[]    findAll()
 * @method ReviewModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{


    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, ReviewModule::class);
    }

    public function getIsOnMainPage()
    {


        return $this->createQueryBuilder('t')
            ->select('t')
            ->where('t.isActive = :true')
            ->setParameter('true', 1)
            ->getQuery()
            ->getResult();

    }
}
