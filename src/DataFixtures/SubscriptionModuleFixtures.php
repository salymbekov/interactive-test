<?php

namespace App\DataFixtures;


use App\Entity\SubscriptionModule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SubscriptionModuleFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $emails = ['arnold@schwarzenegger.com','janklod@vandam.com','sylvester@stallone.com'];
        for($i = 0; $i < 3; $i++){
            $subscriptionModule = new SubscriptionModule();
            $subscriptionModule
                ->setEmail($emails[$i])
                ->setDate(new \DateTime("now"));
                $manager->persist($subscriptionModule);
        }
        $manager->flush();
    }

}