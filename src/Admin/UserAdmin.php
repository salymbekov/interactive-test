<?php

namespace App\Admin;

use App\Entity\User;
use App\Model\Enumeration\ClientTypeEnumeration;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserAdmin extends AbstractAdmin
{

    public function prePersist($object)
    {
        parent::prePersist($object);
        $this->updateUser($object);
    }

    public function updateUser(User $u)
    {
        if ($u->getNewPass()) {
            $u->setPlainPassword($u->getNewPass());
        }

        $um = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $um->updateUser($u, false);
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);
        $this->updateUser($object);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('fullName',null, array('label' => 'Фамилия Имя Отчество'))
            ->add('email')
            ->add('phoneNumber', null, array('label' => 'Телефон'))
            ->add('clientType', null, array('label' => 'Тип клиента'))
            ->add('organization_name', null, array('label' => 'Организация'))
            ->add('roles',null, array('label' => 'Роли'))
            ->add('enabled',null, array('label' => 'Доступность'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('avatar')
            ->add('fullName', null, array('label' => 'Фамилия Имя Отчество'))
            ->add('email')
            ->add('phoneNumber', null, array('label' => 'Телефон'))
            ->add('clientType', null, array('label' => 'Тип клиента'))
            ->add('organization_name', null, array('label' => 'Организация'))
            ->add('roles', null, array('label' => 'Роли'))
            ->add('enabled', null, array('label' => 'Доступность'))
            ->add('_action', null, array(
                'label' => 'Действия',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $roles = $container->getParameter('security.role_hierarchy.roles');
        $rolesChoices = self::flattenRoles($roles);

        $formMapper
            ->add('fullName', TextType::class, array('label' => 'Фамилия Имя Отчество'))
            ->add('email', EmailType::class)
            ->add('phoneNumber', TelType::class, array('label' => 'Телефон'))
            ->add('newPass', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Пароль должен совпадать',
                'required' => false,
                'first_options' => array(
                    'label' => 'Указать новый пароль',
                    'attr' => array(
                        'placeholder' => 'Пароль',
                    ),
                ),
                'second_options' => array(

                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Повтор пароля',
                    ),
                ),
            ))
            ->add('clientType', ChoiceType::class, array('label' => 'Тип клиента', 'choices' => [
                'Коорпоративный клиент' => ClientTypeEnumeration::CORPORATE_CLIENT,
                'Частное лицо' => ClientTypeEnumeration::CORPORATE_CLIENT
            ]))
            ->add('organization_name', TextType::class, array('label' => 'Имя организации',
                'required' => false
            ))
            ->add('roles', ChoiceType::class, array('label' => 'Роль',
                'choices' => $rolesChoices,
                'multiple' => true
            ))
            ->add('enabled',CheckboxType::class, array('label' => 'Доступность'))
            ->add('avatarFile', FileType::class, array('label' => 'Аватар', 'required' => true));

    }

    private static function flattenRoles($rolesHierarchy)
    {
        $flatRoles = array();
        foreach ($rolesHierarchy as $roles) {

            if (empty($roles)) {
                continue;
            }

            foreach ($roles as $role) {
                if (!isset($flatRoles[$role])) {
                    $flatRoles[$role] = $role;
                }
            }
        }

        return $flatRoles;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('fullName',null, array('label' => 'Фамилия Имя Отчество'))
            ->add('email')
            ->add('phoneNumber',null, array('label' => 'Телефон'))
            ->add('clientType',null, array('label' => 'Тип клиента'))
            ->add('organization_name',null, array('label' => 'Имя организации'))
            ->add('roles',null, array('label' => 'Роли'))
            ->add('enabled',null, array('label' => 'Доступность'));
           // ->add('avatar', null, array('label' => 'Аватар', 'template' => 'SonataAdmin/list_mapper_twigs/list_image.html.twig'));
    }
}
