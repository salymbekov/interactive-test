<?php

namespace App\Admin;

use App\Entity\EventSession;
use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class EventBookingAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'event';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('event', null, array('label' => 'Мероприятие'))
            ->add('date', null, array('label' => 'Дата брони'))
            ->add('user', null, array('label' => 'Клиент'))
            ->add('payment', null, array('label' => 'Оплачено'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('user', EntityType::class, array('label' => 'Имя', 'class' => User::class,  'route' => ['name' => 'show']))
            ->add('user.username', null, array('label' => 'Email', 'class' => User::class))
            ->add('date', 'date', array('label' => 'Дата брони', 'format' => 'd.m.Y'))
            ->add('payment', null, array('label' => 'Оплачено', 'editable' => true))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('event.event.eventName', null, array(
                'label' => 'Мероприятие',
                'attr' => array(
                    'readonly' => true,
                )
            ))
            ->add('event.name', null, array(
                'label' => 'Сеанс',
                'attr' => array(
                    'readonly' => true,
                )
            ))
            //->add('event', EntityType::class, array('label' => 'Мероприятие', 'class' => EventSession::class))
            ->add('date', DateType::class, array('label' => 'Дата брони', 'widget' => 'single_text'))
            ->add('user', EntityType::class, array('label' => 'Клиент', 'class' => User::class))
            ->add('payment', null, array('label' => 'Оплачено'));
    }
}