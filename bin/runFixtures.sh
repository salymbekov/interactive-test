#!/usr/bin/env bash

pwd



php bin/console doctrine:database:drop --force

php bin/console doctrine:database:create

php bin/console doctrine:schema:create

cp -r fixturesData/images public/uploads

php bin/console doctrine:fixtures:load -n
