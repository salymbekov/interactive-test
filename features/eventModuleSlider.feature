# language: ru

@eventSliderModule @fixtures
Функционал: Тестируем отображение Мероприятий на главной странице

  @frontEnd
  Сценарий: Поиск признаков модуля мероприятий на главной странице
    Допустим я нахожусь на главной странице
    И Я вижу cлово "Мероприятия" в элементе "h3"
    И Я вижу cлово "Развитие" в элементе "h5"
    И Я вижу cлово "pro" в элементе ".event-type"
    И Я вижу cлово "НЛП-Мастер" в элементе "h5"
    И Я вижу cлово "business" в элементе ".event-type"
    И Я вижу cлово "Эриксоновский гипноз" в элементе "h5"
    И Я вижу cлово "general" в элементе ".event-type"